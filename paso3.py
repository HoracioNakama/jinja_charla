from jinja2 import Environment, FileSystemLoader
import os

BASE_DIR = os.getcwd()

file_loader = FileSystemLoader("templates")
env = Environment(loader=file_loader)

# enlistamos las páginas a generar
paginas_a_generar = ['index_desarrollo.html', 'quienes-somos_desarrollo.html', 'papas.html']
contenidos_a_generar = ['este es el index', 'esto es quienes somos', 'Aquí tenemos papas']


for pagina, contenido in zip(paginas_a_generar, contenidos_a_generar):
    template = env.get_template(pagina)
    contenido = {'titulo': 'paso 3', 'contenido': f'paso 3: {contenido}'}
    output = template.render(contenido)
    with open(os.path.join(BASE_DIR, 'output', pagina.replace('desarrollo', 'produccion')), 'w') as f:
        f.write(output)
        print("Guardado el archivo")


# Recordatorio como trabajar
# 1. Agregar un archivo a la carpeta "templates"
# 2. Colocar su nombre en la variable (lista) "paginas_a_generar"
# 3. Agregarle el contenido en la variable (lista) "contenidos_a_generar"
# 4. Correr el archivo py, y revisar la carpeta "output"