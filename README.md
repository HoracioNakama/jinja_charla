# JINJA CHARLA

## Situación

Un cliente necesita un sitio web que se actualizará periodicamente pero con un bajo presupuesto.

## A tener en cuenta

- Por el presupuesto descartamos frameworks como Django y Flask (requieren VPS)

- Podríamos usar un Generador de Sitios Staticos (Static Site Generator) como Nikola o Pelican.
(Personalmente no puedo porque no domino su sistema de templates)

## Problemas a resolver

- Debemos armar varias páginas web con el mismo template (Ej. Nos piden cambiar el menu)

- Las nuevas páginas no deberían ralentizar nuestro trabajo (Ej. Quieren sacar una nueva página)

## Filosofia

Simple es mejor que complejo (Zen de Python)


## Resolución

### Paso 0. Empecemos con Jinja
Aclaración: comandos para Linux
1. Armamos un entorno virtual: $virtualenv --python=/usr/bin/python3 .env
2. Activamos el entorno virtual: $source .env/bin/activate
3. Instalamos jinja2: $pip3 install jinja2 

### Paso 1. Probemos Jinja

### Paso 2. Probemos armar varias páginas

### Final Recordatorio como trabajar
1. Agregar un archivo a la carpeta "templates"
2. Colocar su nombre en la variable (lista) "paginas_a_generar"
3. Agregarle el contenido en la variable (lista) "contenidos_a_generar"
4. Correr el archivo py, y revisar la carpeta "output"