from jinja2 import Environment, FileSystemLoader
import os

BASE_DIR = os.getcwd()

file_loader = FileSystemLoader("templates")
env = Environment(loader=file_loader)

# enlistamos las páginas a generar
paginas_a_generar = ['index_desarrollo.html', 'quienes-somos_desarrollo.html']

for pagina in paginas_a_generar:
    template = env.get_template(pagina)
    contenido = {'titulo': 'paso 2', 'contenido': 'paso 2: otros saludos'}
    output = template.render(contenido)
    with open(os.path.join(BASE_DIR, 'output', pagina.replace('desarrollo', 'produccion')), 'w') as f:
        f.write(output)
        print("Guardado el archivo")


# Si quisieran cambiar el contenido de la página
# contenidos_a_generar = ['este es el index', 'esto es quienes somos']
# for pagina, contenido in zip(paginas_a_generar, contenidos_a_generar):
#     template = env.get_template(pagina)
#     contenido = {'titulo': 'paso 2', 'contenido': f'paso 2: {contenido}'}
#     output = template.render(contenido)
#     with open(os.path.join(BASE_DIR, 'output', pagina.replace('desarrollo', 'produccion')), 'w') as f:
#         f.write(output)
#         print("Guardado el archivo")