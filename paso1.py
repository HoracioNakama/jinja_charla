from jinja2 import Environment, FileSystemLoader
import os

BASE_DIR = os.getcwd()

file_loader = FileSystemLoader("templates")
env = Environment(loader=file_loader)

template = env.get_template('index_desarrollo.html')

contenido = {'titulo': 'paso 1', 'contenido': 'Este es un ejemplo de Jinja'}

output = template.render(contenido)
# print(output)

with open(os.path.join(BASE_DIR, 'output', 'index_produccion.html'), 'w') as f:
    f.write(output)
    print("Guardado el archivo")